

export default function Banner() {

	return (

		<div className="container-fluid mt-2">

			<div className="row justify-content-center">

				<div className = "col-10 col-md-8">

					<div className="jumbotron">

						<h1>
							Welcome to React-Booking
						</h1>

						<p>
							Opportunities for everyone and everywhere
						</p>
						
						<button className="btn btn-primary">
							Enroll Now
						</button>
						
					</div>
					
				</div>
				
			</div>
			
		</div>
	)
}