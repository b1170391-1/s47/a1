
import courseData from "./../data/courseData"

/*Components*/
import CourseCard from "./../components/CourseCard"

export default function Courses() {
	console.log(courseData)

	const courses = courseData.map(course => {
		return (
			<CourseCard key={course.id} courseProp={course}/>
		)
	})

	return (
		<>
			{courses}
		</>
	)
}