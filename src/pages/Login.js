import {useState, useEffect} from "react";

import {
	Container,
	Row,
	Col,
	Form,
	Button

} from "react-bootstrap"

export default function Login() {
	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")

	const [isDisabled, setIsDisabled] = useState(true)

	function loginUser(e){
		e.preventDefault()

		setEmail("")
		setPassword("")

		alert("User logged in")
	}

	useEffect ( () => {
		if (email !== "" && password !== "" )
			
		{
			setIsDisabled(false)
		}
		else 
		{
			setIsDisabled(true)
		}
	}, [email, password])

	return (
		<Container className = "my-5">
			<Row className="justify-content-center">
				<Col xs={10} md={6}>
						<Form 
							className="border p-3"
							onSubmit = {(e) => loginUser(e)}
						>
					{/*Email*/}
						  <Form.Group className="mb-3" controlId="formBasicEmail">
						    <Form.Label>Email address</Form.Label>
						    <Form.Control 
						    	type="email" 
						    	placeholder="Enter email"
						    	value = {email}
						    	onChange={ (e) => {
						    		setEmail(e.target.value)
						    	}}
						     />
						  </Form.Group>

						{/*password*/}
						  <Form.Group className="mb-3" controlId="formBasicPassword">
						    <Form.Label>Password</Form.Label>
						    <Form.Control type="password" placeholder="Password"
						    	 value={password}
						    	 onChange = { (e) => {
						    	 	setPassword (e.target.value)
						    	 }}
						    />
						  </Form.Group>
						
							{/*Submit*/}
						  <Button variant="primary" type="submit"
						  	disabled = {isDisabled}
						  >
						    Submit
						  </Button>
						</Form>
				</Col>
			</Row>
		</Container>
	)
}